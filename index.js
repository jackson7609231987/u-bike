window.encodeURIComponent = function (value) {
  var map = {
    ' ': '%20',
    '#': '%23',
    '%': '%25',
    '&': '%26',
    '+': '%2B',
    '/': '%2F',
    '\'': '\'\'', // double for wio informix sql insert
    '=': '%3D',
    '?': '%3F',
    '\r': '%0D',
    '\n': '%0A'
  };

  return value.replace(/[ #%&+/=''?\r\n]/g, function (c) {
    return map[c];
  });
};

(function () {
  window.notJSON = function (data) {
    try {
      $.parseJSON(data);
      return false;
    } catch (err) {
      return true;
    }
  }
})();



$.ajaxSetup({
  cache: false
});



//  var domain = 'http://gfcweb/xml/xml/srvd140w_api.html?';


function search(domain) {
  $.get(domain).done(function (data) {
    $.each(data, function (i, el) {
      var lat = data[i].lat;
      var lng = data[i].lng;
      var ar = data[i].ar;
      data[i].ar = '<a href=https://www.google.com.tw/maps/place/' + lat + ',' + lng + ' ' + 'class="pointer center " style="color:DodgerBlue;" target="_blank" >' + ar + '</a>'

    });


    var table = $('#data-table').dataTable({
      "data": data,
      "processing": true,
      "destroy": true,
      // "dom": 'lfBrtip',
      // "buttons": [{
      //   "extend": "excel",
      //   "text": "匯出excel檔",
      //   "title": "合約號機地址經緯度" + date
      // }],
      "columns": [{
          "title": "場站中文名稱",
          "sClass": "align-center",
          "width": "10%",
          "data": "sna"
        },
        {
          "title": "場站總停車格",
          "sClass": "align-center",
          "width": "10%",
          "data": "tot"
        },
        {
          "title": "場站目前車輛數量",
          "sClass": "align-center",
          "width": "10%",
          "data": "sbi"
        },
        {
          "title": "空位數量",
          "sClass": "align-center",
          "width": "10%",
          "data": "bemp"
        },
        {
          "title": "資料更新時間",
          "sClass": "align-center",
          "data": "mday"
        },
        {
          "title": "地點",
          "sClass": "align-center",
          "data": "ar"
        }
      ],
      "language": {
        "sProcessing": "處理中...",
        "sLengthMenu": "顯示 _MENU_ 項結果",
        "sZeroRecords": "沒有匹配結果",
        "sInfo": "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
        "sInfoEmpty": "顯示第 0 至 0 項結果，共 0 項",
        "sInfoFiltered": "(從 _MAX_ 項結果過濾)",
        "sInfoPostFix": "",
        "sSearch": "搜尋:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "首頁",
          "sPrevious": "上頁",
          "sNext": "下頁",
          "sLast": "尾頁"
        }
      }
    });


  });

}


$(document).ready(function () {
  var dt = new Date();
  var month = dt.getMonth() + 1;
  var day = dt.getDate();
  var date = dt.getFullYear().toString() + "-" + (month < 10 ? "0" : "") + month + "-" + (day < 10 ? "0" : "") + day;

  var currDate = Date.parse(new Date().toDateString());

  var opt = {
    dateFormat: 'yy-mm-dd',
    showSecond: true,
    timeFormat: 'HH:mm:ss'
  };
  //需要時間顯示就html指定id
  // $('#datetimepicker_s').datetimepicker(opt);
  // $('#datetimepicker_e').datetimepicker(opt);
  // $('#datetimepicker_d').datetimepicker(opt);
  // $('#datetimepicker_i').datetimepicker(opt);

  // var allowUpdate_ct = 'N';

  https: //www.google.com.tw/maps/place/
    setInterval(function () {
      var domain = 'https://tcgbusfs.blob.core.windows.net/dotapp/youbike/v2/youbike_immediate.json';
      search(domain);
    }, 15000);





});